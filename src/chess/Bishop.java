package src.chess;

/**
 * @author Beomsik Kim
 * @author Wilson Peralta
 */

/**
 * Class bishop extends the Pieces class with specific functions for functionality
 * to the bishop piece on the board
 * @param locat string with desired location
 * @param givenSide what color of the piece
 */

public class Bishop extends Pieces {
	public Bishop(String locat, String givenSide){
		this.side=givenSide;
		locNow=locat;
	}
/**
 * method isPathClear checks whether desired path between src and dest is clear
 * @param string f_pos
 * @return a boolean true or false accordingly
 */
	public boolean isPathClear(String f_pos){
		int[] givenArr=convStrIntoIntArr(f_pos);
		int[] currArr=convStrIntoIntArr(this.locNow);
		if(!validMove(givenArr)){
			return false;
		}
		else if((Math.abs(givenArr[0]-currArr[0]))==(Math.abs(givenArr[1]-currArr[1]))){
			return true;
		}
		else if((givenArr[0]==currArr[0])||(givenArr[1]==currArr[1])){
			return false;
		}
		else{
			return false;
		}
	}

	/**
	 * toString method 
	 * @return proper color of the piece
	 */
	public String toString(){
		if(side.equals("black")){
			return "bB";
		}
		else{
			return "wB";
		}
	}
}
