package src.chess;

/**
 * @author Beomsik Kim
 * @author Wilson Peralta
 */

public class Pieces {
	public String side;
	public String locNow;
	public boolean isPathClear(String f_pos){
		return false;
	}
	/**
	 * method validMove takes in an array of ints and checks whether selected move is within the range
	 * @param int array
	 * @return a boolean true or false accordingly 
	 */
	public boolean validMove(int[] arrInput){
		if(arrInput[1]<1 || arrInput[1]>8 || arrInput[0]<1 || arrInput[0]>8){
			return false;
		}
		else{
			return true;
		}
	}
	
	/**
	 * method convStrIntoIntArr converts input and allocates it into an int array
	 * @param string location of the piece
	 * @return int array with the location
	 */
	public int[] convStrIntoIntArr(String locat){
		locat = locat.toLowerCase();
		int[] newIntArr = new int[2];
		newIntArr[0] = -1;
		newIntArr[1] = -1;
		if (locat.length() != 2) {
			return newIntArr;
		}
		else{
			int charIntoInt = locat.charAt(0)+1-'a';
			newIntArr[1] = Integer.parseInt(locat.charAt(1)+"");
			newIntArr[0] = charIntoInt;
			return newIntArr;
		}
	}
}