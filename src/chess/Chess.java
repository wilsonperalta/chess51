package src.chess;

/**
 * @author Beomsik Kim
 * @author Wilson Peralta
 * Main Chess file for execution.
 */

import java.util.Scanner;
public class Chess {
	/**
	 * @param args Main arguments, but null in our case
	 */
	public static void main(String[] args) {
		String desiredLocation;
		String currTurn="White's move:";
		String finalist="";
		Boolean checkStatus=false;
		Boolean moveIsValid=false;
		Boolean optedForDraw=false;
		Boolean resultedInDraw=false;
		Gamefield mainfield=new Gamefield();
		Scanner scanner=new Scanner(System.in);
		while(true){
			System.out.println("");
			mainfield.generateGamefield();
			System.out.println("");
			/**
			 * Check for checks
			 */
			if(checkStatus){
				if (mainfield.determineIfCheckmate()){
					System.out.println("Checkmate");
					if(currTurn.equals("White's move:")){
						finalist="White wins";
					}
					else {
						finalist="Black wins";
					}
					break;
				}
				else{
					System.out.println("Check");
				}
			}
			System.out.print(currTurn);
			String givenMove=scanner.nextLine();
			/**
			 * Check for draw
			 */
			if (optedForDraw){
				resultedInDraw=true;
				break;
			}
			/**
			 * Check for resign
			 */
			else if (givenMove.equals("resign")){
				if(currTurn.equals("White's move:")){
					finalist="Black wins";
				}
				else {
					finalist="White wins";
				}
				break;
			}
			else if (!optedForDraw&&givenMove.equals("draw")){
				moveIsValid=false;
				System.out.println("Illegal move, try again");
				continue;
			}
			String[] givenMoveArr=givenMove.split("\\s+");
			if(!(mainfield.checkIfValidBool(givenMoveArr[0]))||!(mainfield.checkIfValidBool(givenMoveArr[1]))){
				System.out.println("Illegal move, try again");
				continue;
			}
			/**
			 * Check for invalid input
			 */
			if(givenMoveArr.length!=2){
				if(givenMoveArr.length!=3){
					System.out.println("Illegal move, try again");
					moveIsValid = false;
					continue;
				}
			}
			Pieces currPiece=mainfield.choosePiece(givenMoveArr[0]);
			desiredLocation=givenMoveArr[1];
			if (currPiece==null){
				System.out.println("Illegal move, try again");
				moveIsValid=false;
				continue;
			}
			if(currTurn.equals("Black's move:")){
				if (currPiece.side.equals("white")){
					moveIsValid=false;
					System.out.println("Illegal move, try again");
					continue;
				}
			}
			else{
				if (currPiece.side.equals("black")){
					moveIsValid=false;
					System.out.println("Illegal move, try again");
					continue;
				}
			}
			if(!currPiece.isPathClear(desiredLocation)||!mainfield.determineIfPathClear(currPiece, desiredLocation)){
				/**
				 * Check for Castling
				 */
				if((currPiece instanceof King)){
					King subKingPiece=(King)currPiece;
					if(subKingPiece.firstMove != true){
						System.out.println("Illegal move, try again");
						continue;
					}
					if(currPiece.side.equals("white")){
						switch(desiredLocation){
							case "g1":
								if((mainfield.choosePiece("f1")!=null)||(mainfield.choosePiece("g1")!=null)||(mainfield.choosePiece("h1")==null)){
									System.out.println("Illegal move, try again");
									continue;
								}
								Rook subRookPiece=(Rook)mainfield.choosePiece("h1");
								if(subRookPiece.firstMove != true){
									System.out.println("Illegal move, try again");
									continue;
								}
								else{
									mainfield.executePieceMove("e1", desiredLocation, 'x');
									mainfield.executePieceMove("h1", "f1", 'x');
									if(currTurn.equals("White's move:")){
										currTurn="Black's move:";
									}
									else{
										currTurn="White's move:";
									}
									continue;
								}
							case "c1":
								if((mainfield.choosePiece("d1")!=null)||(mainfield.choosePiece("c1")!=null)||(mainfield.choosePiece("b1") != null)||(mainfield.choosePiece("a1")==null)){
									System.out.println("Illegal move, try again");
									continue;
								}
								Rook subRookPiece2=(Rook) mainfield.choosePiece("a1");
								if(subRookPiece2.firstMove != true){
									System.out.println("Illegal move, try again");
									continue;
								}
								else{
									mainfield.executePieceMove("e1", desiredLocation, 'x');
									mainfield.executePieceMove("a1", "d1", 'x');
									if(currTurn.equals("White's move:")){
										currTurn = "Black's move:";
									}
									else{
										currTurn = "White's move:";
									}
									continue;
								}
							default:
								System.out.println("Illegal move, try again");
								continue;
						}
					}
					else{
						switch(desiredLocation){
							case "g8":
								if((mainfield.choosePiece("f8") != null) || (mainfield.choosePiece("g8") != null) ||(mainfield.choosePiece("h8") == null)){
									System.out.println("Illegal move, try again");
									continue;
								}
								Rook subRookPiece3 = (Rook) mainfield.choosePiece("h8");
								if(subRookPiece3.firstMove != true){
									System.out.println("Illegal move, try again");
									continue;
								}
								else{
									mainfield.executePieceMove("e8", desiredLocation, 'x');
									mainfield.executePieceMove("h8", "f8", 'x');
									if(currTurn.equals("White's move:")){
										currTurn = "Black's move:";
									}
									else{
										currTurn = "White's move:";
									}
									continue;
								}
							case "c8":
								if((mainfield.choosePiece("d8") != null) || (mainfield.choosePiece("c8") != null) || (mainfield.choosePiece("b8") != null) || (mainfield.choosePiece("a8") == null)){
									System.out.println("Illegal move, try again");
									continue;
								}
								Rook subRookPiece4 = (Rook) mainfield.choosePiece("a8");
								if(subRookPiece4.firstMove != true){
									System.out.println("Illegal move, try again");
									continue;
								}
								else{
									mainfield.executePieceMove("e8", desiredLocation, 'x');
									mainfield.executePieceMove("a8", "d8", 'x');
									if(currTurn.equals("White's move:")){
										currTurn = "Black's move:";
									}
									else{
										currTurn = "White's move:";
									}
									continue;
								}
							default:
								System.out.println("Illegal move, try again");
								continue;
						}
					}
				}
				if(!(currPiece instanceof Pawn)){
					System.out.println("Illegal move, try again");
					moveIsValid = false;
					continue;
				}
				/**
				 * Check for Enpassant
				 */
				else {

					if(mainfield.choosePiece(desiredLocation) == null){

						int tempInt;
						if(currPiece.side.equals("white")){
							tempInt = (Character.getNumericValue(desiredLocation.charAt(1)))-1;
						}
						else{
							tempInt = (Character.getNumericValue(desiredLocation.charAt(1)))+1;
						}

						String detDesiredLocation = desiredLocation.charAt(0)+(Integer.toString(tempInt));

						if((mainfield.choosePiece(detDesiredLocation) != null) && ((mainfield.choosePiece(detDesiredLocation) instanceof Pawn))){
							Pawn subPawnPiece = (Pawn) (mainfield.choosePiece(detDesiredLocation));
							if(subPawnPiece.advancedTwo == true){
								mainfield.pawnSpecialEatingCase(givenMoveArr[0], desiredLocation, detDesiredLocation);
								if(currTurn.equals("White's move:")){
									currTurn = "Black's move:";
								}
								else{
									currTurn = "White's move:";
								}
								continue;
							}
						}

						System.out.println("Illegal move, try again");
						moveIsValid = false;
						continue;
					}

					Pieces desiredPiece = mainfield.choosePiece(desiredLocation);

					int[] currPawnLoc = currPiece.convStrIntoIntArr(currPiece.locNow);
					int[] currPawnEatingPath = desiredPiece.convStrIntoIntArr(desiredLocation);

					if(Math.abs(currPawnLoc[0]- currPawnEatingPath[0])!=1 ||
							Math.abs(currPawnLoc[1]- currPawnEatingPath[1])!=1){
						System.out.println("Illegal move, try again");
						moveIsValid = false;
						continue;
					}

				}
			}

			/**
			 * Check to see if piece is eating teammate
			 */
			if(currTurn.equals("White's move:")){
				if((mainfield.choosePiece(desiredLocation) != null) && (mainfield.choosePiece(desiredLocation)).side.equals("white")){
					System.out.println("Illegal move, try again");
					moveIsValid = false;
					continue;
				}
			} else{
				if(((mainfield.choosePiece(desiredLocation) != null) && (mainfield.choosePiece(desiredLocation)).side.equals("black"))){
					System.out.println("Illegal move, try again");
					moveIsValid = false;
					continue;
				}
			}

			/**
			 * Check if pawn is trying to eat in straight line which is not legal
			 */
			if(currTurn.equals("White's move:")){
				if((currPiece instanceof Pawn) && (mainfield.choosePiece(desiredLocation) != null) && (mainfield.choosePiece(desiredLocation)).side.equals("black")){
					if(givenMoveArr[0].charAt(0)== givenMoveArr[1].charAt(0)){
						System.out.println("Illegal move, try again");
						moveIsValid = false;
						continue;
					}
				}
			} else{
				if(((currPiece instanceof Pawn) && (mainfield.choosePiece(desiredLocation) != null) && (mainfield.choosePiece(desiredLocation)).side.equals("white"))){
					if(givenMoveArr[0].charAt(0)== givenMoveArr[1].charAt(0)){
						System.out.println("Illegal move, try again");
						moveIsValid = false;
						continue;
					}
				}
			}
			/**
			 * Check for pawn promo with extra input
			 */
			if(givenMoveArr.length == 3){
				if(givenMoveArr[2].equals("draw?")){
					optedForDraw = true;
				}
				else if((desiredLocation.charAt(1) == '8' || desiredLocation.charAt(1) == '1') && (currPiece instanceof Pawn)){
					switch(givenMoveArr[2]){
						case "K":
							if(!mainfield.executePieceMove(givenMoveArr[0], givenMoveArr[1], 'K')) {
								System.out.println("Illegal move, try again");
								continue;
							}
							else if(currTurn.equals("White's move:")){
								currTurn = "Black's move:";
							}
							else{
								currTurn = "White's move:";
							}
							continue;
						case "B":
							if(!mainfield.executePieceMove(givenMoveArr[0], givenMoveArr[1], 'B')) {
								System.out.println("Illegal move, try again");
								continue;
							}
							else if(currTurn.equals("White's move:")){
								currTurn = "Black's move:";
							}
							else{
								currTurn = "White's move:";
							}
							continue;
						case "Q":
							boolean resultOfExecution = mainfield.executePieceMove(givenMoveArr[0], givenMoveArr[1], 'Q');
							if(!resultOfExecution){
								System.out.println("Illegal move, try again");
								continue;
							}
							else if(currTurn.equals("White's move:")){
								currTurn = "Black's move:";
							}
							else{
								currTurn = "White's move:";
							}
							continue;
						case "N":
							if(!mainfield.executePieceMove(givenMoveArr[0], givenMoveArr[1], 'N')) {
								System.out.println("Illegal move, try again");
								continue;
							}
							else if(currTurn.equals("White's move:")){
								currTurn = "Black's move:";
							}
							else{
								currTurn = "White's move:";
							}
							continue;
						case "R":
							if(!mainfield.executePieceMove(givenMoveArr[0], givenMoveArr[1], 'R')) {
								System.out.println("Illegal move, try again");
								continue;
							}
							else if(currTurn.equals("White's move:")){
								currTurn = "Black's move:";
							}
							else{
								currTurn = "White's move:";
							}
							continue;
						default:
							System.out.println("Illegal move, try again");
							continue;
					}
				}
				else {
					System.out.println("Illegal move, try again");
					continue;
				}
			}
			/**
			 * Check for pawn promo without extra input
			 */
			if((givenMoveArr.length != 3) && (currPiece instanceof Pawn) && (desiredLocation.charAt(1) == '8' || desiredLocation.charAt(1) == '1')){
				if(!mainfield.executePieceMove(givenMoveArr[0], givenMoveArr[1], 'Q')){
					System.out.println("Illegal move, try again");
					continue;
				}
				moveIsValid = false;
				if(currTurn.equals("White's move:")){
					currTurn = "Black's move:";
				}
				else{
					currTurn = "White's move:";
				}
				continue;
			}

			moveIsValid = true;
			if (moveIsValid) {
				boolean resultOfExecution2 = mainfield.executePieceMove(givenMoveArr[0], givenMoveArr[1], 'x');
				if(!resultOfExecution2){
					System.out.println("Illegal move, try again");
					continue;
				}
				else if(currTurn.equals("White's move:")){
					if(mainfield.determineIfCheck("white")){
						checkStatus = true;
					}
					else {
						checkStatus = false;
					}
					currTurn = "Black's move:";
				}
				else {
					if(mainfield.determineIfCheck("black")){
						checkStatus = true;
					}
					else {
						checkStatus = false;
					}
					currTurn = "White's move:";
				}
			}
			moveIsValid = false;
		}
		if (resultedInDraw) {
		}
		else {
			System.out.println(finalist);
		}
		scanner.close();
	}

	/**
	 * @param givenInt  Integer value to be converted into Character
	 * @return The converted Character value
	 */
	public static char convIntIntoChar(int givenInt){
		return (char)(97+givenInt);
	}
}
