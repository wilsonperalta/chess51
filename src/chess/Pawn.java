package src.chess;

/**
 * @author Beomsik Kim
 * @author Wilson Peralta
 */



/**
 * Class pawn extends the Pieces class with specific functions for functionality
 * to the pawn piece on the board
 * @param locat string with desired location
 * @param givenSide what color of the piece
 */
public class Pawn extends Pieces {
	public boolean firstMove;
	public boolean advancedTwo;
	public Pawn(String locat, String givenSide){
		this.side=givenSide;
		locNow=locat;
		advancedTwo=false;
		firstMove=true;
	}

	
	/**
	 * 
	 * method isPathClear checks whether desired path between src and dest is clear
	 * @param string f_pos
	 * @return a boolean true or false accordingly
	 */
	public boolean isPathClear(String f_pos){
		int[] currArr=convStrIntoIntArr(f_pos);
		int[] givenArr=convStrIntoIntArr(this.locNow);
		if(!validMove(currArr)){
			return false;
		}
		else if(currArr[0]!=givenArr[0]){
			return false;
		}
		else if(givenArr[0]==currArr[0]){
			if(givenArr[1]==currArr[1]){
				return false;
			}
			else if (side.equals("white")&&currArr[1]-givenArr[1]==1){
				return true;
			}
			else if(side.equals("black")&&currArr[1]-givenArr[1]==-1){
				return true;
			}
			else if(firstMove){
				if(side.equals("black")&&currArr[1]-givenArr[1]==-2){
					return true;
				}
				else if (side.equals("white")&&currArr[1]-givenArr[1]==2){
					return true;
				}
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
		return false;
	}

	/**
	 * method setFirstMove checks whether pawn has already moved
	 * to allow 1 or 2 steps accordingly
	 * @param a boolean of whether it moved
	 * 
	 */
	public void setFirstMove(boolean givenMoved){
		this.firstMove = givenMoved;
	}

	public String toString(){
		if (side.equals("black")){
			return "bp";
		}
		else{
			return "wp";
		}
	}
}
