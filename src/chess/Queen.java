package src.chess;

/**
 * @author Beomsik Kim
 * @author Wilson Peralta
 */

/**
 * Class queen extends the Pieces class with specific functions for functionality
 * to the queen piece on the board
 * @param locat string with desired location
 * @param givenSide what color of the piece
 */
public class Queen extends Pieces {
	public Queen(String locat, String givenSide){
		this.side=givenSide;
		locNow=locat;
	}
	/**
	 * method isPathClear checks whether desired path between src and dest is clear
	 * @param string f_pos
	 * @return a boolean true or false accordingly
	 */

	public boolean isPathClear(String f_pos){
		int[] currArr=convStrIntoIntArr(f_pos);
		int[] givenArr=convStrIntoIntArr(this.locNow);
		if(!validMove(currArr)){
			return false;
		}
		
		else if((Math.abs(currArr[0]-givenArr[0]))==(Math.abs(currArr[1]-givenArr[1]))){
			return true;
		}
		/*
		 * checks whether position is the same
		 */
		else if(givenArr[0]==currArr[0]){
			if(givenArr[1]==currArr[1]){
				return false;
			}
			else {
				return true;
			}
		}
		else if(givenArr[1]==currArr[1]){
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * toString method 
	 * @return proper color of the piece
	 */
	public String toString(){
		if (side.equals("black")){
			return "bQ";
		}
		else{
			return "wQ";
		}
	}
}
