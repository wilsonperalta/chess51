package src.chess;

/**
 * @author Beomsik Kim
 * @author Wilson Peralta
 * 
 */

/**
 * Class king extends the Pieces class with specific functions for functionality
 * to the king piece on the board
 * @param locat string with desired location
 * @param givenSide what color of the piece
 */
public class King extends Pieces {
	public boolean firstMove;
	public King(String locat, String givenSide){
		this.side=givenSide;
		locNow=locat;
		firstMove=true;
	}
	
	/**
	 * method isPathClear checks whether desired path between src and dest is clear
	 * @param string f_pos
	 * @return a boolean true or false accordingly
	 */

	public boolean isPathClear(String f_pos){
		int[] currArr=convStrIntoIntArr(f_pos);
		int[] givenArr=convStrIntoIntArr(this.locNow);
		int largeTarget=givenArr[1]+1;
		int smallTarget=givenArr[1]-1;
		int val1=givenArr[1];
		if(!validMove(currArr)){
			return false;
		}
		/*
		 * checks whether position is the same
		 */
		else if(currArr[0]==givenArr[0]){
			if(currArr[1]==givenArr[1]){
				return false;
			}
			else if(currArr[1]==largeTarget||currArr[1]==smallTarget){
				return true;
			}
		}
		else if(currArr[0]==givenArr[0]+1||currArr[0]==givenArr[0]-1){
			if(currArr[1]==val1||currArr[1]==largeTarget||currArr[1]==smallTarget){
				return true;
			}
		}
		else{
			return false;
		}
		return false;
	}
	
	/**
	 * toString method 
	 * @return proper color of the piece
	 */

	public String toString(){
		if (side.equals("black")){
			return "bK";
		}
		else{
			return "wK";
		}
	}
}
