package src.chess;

/**
 * @author Beomsik Kim
 * @author Wilson Peralta
 */


/**
 * Class rook extends the Pieces class with specific functions for functionality
 * to the rook piece on the board
 * @param locat string with desired location
 * @param givenSide what color of the piece
 */

public class Rook extends Pieces {
	public boolean firstMove;
	/**
	 * @param locat current location of Rook
	 * @param givenSide the given side of chess game
	 */
	public Rook(String locat, String givenSide){
		this.side=givenSide;
		locNow=locat;
		firstMove=true;
	}

	/**
	 * method isPathClear checks whether desired path between src and dest is clear
	 * @param string f_pos
	 * @return a boolean true or false accordingly
	 */
	public boolean isPathClear(String f_pos){
		int[] givenArr=convStrIntoIntArr(f_pos);
		int[] currArr=convStrIntoIntArr(this.locNow);
		if(!validMove(givenArr)){
			return false;
		}
		else if(currArr[0]==givenArr[0]){
			if(currArr[1]==givenArr[1]){
				return false;
			}
			else{
				return true;
			}
		}
		else if(currArr[1]==givenArr[1]){
			return true;
		}
		else{
			return false;
		}
	}
	
	/**
	 * toString method 
	 * @return proper color of the piece
	 */

	public String toString(){
		if (side.equals("black")){
			return "bR";
		}
		else{
			return "wR";
		}
	}
}
