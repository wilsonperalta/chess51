package src.chess;

/**
 * @author Beomsik Kim
 * @author Wilson Peralta
 */

/**
 * Gamefield class with helping methods for Chess.java
 */
public class Gamefield {
    /**
     * whiteKingLocat The location of White King in int array
     * blackKingLocat The location of Black King in int array
     * gameField1 The main board for our Chess game
     */
    int[] whiteKingLocat;
    int[] blackKingLocat;
    Pieces[][] gameField1;
    public Gamefield() {
        blackKingLocat = new int[2];
        blackKingLocat[0] = 0;
        blackKingLocat[1] = 4;
        whiteKingLocat = new int[2];
        whiteKingLocat[0] = 7;
        whiteKingLocat[1] = 4;

        this.gameField1 = new Pieces[8][8];
        createGamefield();
    }
    /**
     * @param givenSide The given side in White or Black
     * @return boolean on whether given side's king is stuck
     */
    public boolean determineIfKingStuck(String givenSide) {
        Pieces[][] simCheckField = new Pieces[8][8];
        Gamefield tempField = new Gamefield();
        tempField.gameField1 = simCheckField;
        for (int m = 0; m < 8; m++) {
            for (int n = 0; m < 8; m++) {
                simCheckField[m][n] = (Pieces) gameField1[m][n];
            }
        }
        tempField.prepBothK();
        if (givenSide.equals("white")) {
            int kRow = tempField.whiteKingLocat[0];
            int kCol = tempField.whiteKingLocat[1];
            int source[] = new int[2];
            int desiredLoc[] = new int[2];
            source[0] = kRow;
            source[1] = kCol;
            if (kRow + 1 <= 7) {
                if (simCheckField[kRow + 1][kCol] == null) {
                    desiredLoc[0] = kRow + 1;
                    desiredLoc[1] = kCol;
                    tempField.executePieceMove(source, desiredLoc);
                    if (!tempField.determineIfCheck("black"))
                        return true;
                }
                if (kCol + 1 <= 7 && simCheckField[kRow + 1][kCol + 1] == null) {
                    desiredLoc[0] = kRow + 1;
                    desiredLoc[1] = kCol + 1;
                    tempField.executePieceMove(source, desiredLoc);
                    if (!tempField.determineIfCheck("black"))
                        return true;
                }
                if (kCol + 1 <= 7 && simCheckField[kRow][kCol + 1] == null) {
                    desiredLoc[0] = kRow;
                    desiredLoc[1] = kCol + 1;
                    tempField.executePieceMove(source, desiredLoc);
                    if (!tempField.determineIfCheck("black"))
                        return true;
                }
                if (kCol - 1 >= 0 && simCheckField[kRow + 1][kCol - 1] == null) {
                    desiredLoc[0] = kRow + 1;
                    desiredLoc[1] = kCol - 1;
                    tempField.executePieceMove(source, desiredLoc);
                    if (!tempField.determineIfCheck("black"))
                        return true;
                }
                if (kCol - 1 >= 0 && simCheckField[kRow][kCol - 1] == null) {
                    desiredLoc[0] = kRow;
                    desiredLoc[1] = kCol - 1;
                    tempField.executePieceMove(source, desiredLoc);
                    if (!tempField.determineIfCheck("black"))
                        return true;
                }
            }
            if (kRow - 1 >= 0) {
                if (simCheckField[kRow - 1][kCol] == null) {
                    desiredLoc[0] = kRow - 1;
                    desiredLoc[1] = kCol;
                    tempField.executePieceMove(source, desiredLoc);
                    if (!tempField.determineIfCheck("black"))
                        return true;
                }
                if (kCol + 1 <= 7 && simCheckField[kRow - 1][kCol + 1] == null) {
                    desiredLoc[0] = kRow - 1;
                    desiredLoc[1] = kCol + 1;
                    tempField.executePieceMove(source, desiredLoc);
                    if (!tempField.determineIfCheck("black"))
                        return true;
                }
                if (kCol - 1 >= 0 && simCheckField[kRow - 1][kCol - 1] == null) {
                    desiredLoc[0] = kRow - 1;
                    desiredLoc[1] = kCol - 1;
                    tempField.executePieceMove(source, desiredLoc);
                    if (!tempField.determineIfCheck("black"))
                        return true;
                }
            }
            return false;
        }
        else {
            int kRow = tempField.blackKingLocat[0];
            int kCol = tempField.blackKingLocat[1];
            int source[] = new int[2];
            int desiredLoc[] = new int[2];
            source[0] = kRow;
            source[1] = kCol;
            if (kRow + 1 <= 7) {
                if (simCheckField[kRow + 1][kCol] == null) {
                    desiredLoc[0] = kRow + 1;
                    desiredLoc[1] = kCol;
                    tempField.executePieceMove(source, desiredLoc);
                    if (!tempField.determineIfCheck("white"))
                        return true;
                }
                if (kCol + 1 <= 7 && simCheckField[kRow + 1][kCol + 1] == null) {
                    desiredLoc[0] = kRow + 1;
                    desiredLoc[1] = kCol + 1;
                    tempField.executePieceMove(source, desiredLoc);
                    if (!tempField.determineIfCheck("white"))
                        return true;
                }
                if (kCol + 1 <= 7 && simCheckField[kRow][kCol + 1] == null) {
                    desiredLoc[0] = kRow;
                    desiredLoc[1] = kCol + 1;
                    tempField.executePieceMove(source, desiredLoc);
                    if (!tempField.determineIfCheck("white"))
                        return true;
                }
                if (kCol - 1 >= 0 && simCheckField[kRow + 1][kCol - 1] == null) {
                    desiredLoc[0] = kRow + 1;
                    desiredLoc[1] = kCol - 1;
                    tempField.executePieceMove(source, desiredLoc);
                    if (!tempField.determineIfCheck("white"))
                        return true;
                }
                if (kCol - 1 >= 0 && simCheckField[kRow][kCol - 1] == null) {
                    desiredLoc[0] = kRow;
                    desiredLoc[1] = kCol - 1;
                    tempField.executePieceMove(source, desiredLoc);
                    if (!tempField.determineIfCheck("white"))
                        return true;
                }
            }
            if (kRow - 1 >= 0) {
                if (simCheckField[kRow - 1][kCol] == null) {
                    desiredLoc[0] = kRow - 1;
                    desiredLoc[1] = kCol;
                    tempField.executePieceMove(source, desiredLoc);
                    if (!tempField.determineIfCheck("white"))
                        return true;
                }
                if (kCol + 1 <= 7 && simCheckField[kRow - 1][kCol + 1] == null) {
                    desiredLoc[0] = kRow - 1;
                    desiredLoc[1] = kCol + 1;
                    tempField.executePieceMove(source, desiredLoc);
                    if (!tempField.determineIfCheck("white"))
                        return true;
                }
                if (kCol - 1 >= 0 && simCheckField[kRow - 1][kCol - 1] == null) {
                    desiredLoc[0] = kRow - 1;
                    desiredLoc[1] = kCol - 1;
                    tempField.executePieceMove(source, desiredLoc);
                    if (!tempField.determineIfCheck("white"))
                        return true;
                }
            }
            return false;
        }
    }
    /**
     * @param desiredLoc Location where player desires to put piece into
     * @param Pieces Piece chosen by player to move
     * @return boolean on whether path from source to desired location by the chosen piece is clear
     */
    public boolean determineIfPathClear(Pieces Pieces, String desiredLoc) {
        if (!Pieces.isPathClear(desiredLoc)){
            return false;
        }
        String desiredLet = desiredLoc.substring(0, 1);
        String desiredNum = desiredLoc.substring(1, 2);

        String currLet = Pieces.locNow.substring(0, 1);
        String currNum = Pieces.locNow.substring(1, 2);

        int desIntX = 8 - Integer.parseInt(desiredNum);
        int desIntY = desiredLet.charAt(0) - 'a';

        int currIntX = 8 - Integer.parseInt(currNum);
        int currIntY = currLet.charAt(0) - 'a';

        if (desIntX == currIntX) {
            if (desIntY < currIntY) {
                for (int j = desIntY + 1; j < currIntY; j++) {
                    if (gameField1[currIntX][j] != null) {
                        return false;
                    }
                }
                return true;
            }
            else if (desIntY > currIntY) {
                for (int j = currIntY + 1; j < desIntY; j++) {
                    if (gameField1[currIntX][j] != null) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        }
        if (desIntY == currIntY) {
            if (desIntX < currIntX) {
                for (int i = desIntX + 1; i < currIntX; i++) {
                    if (gameField1[i][currIntY] != null) {
                        return false;
                    }
                }
                return true;
            }
            else if (desIntX > currIntX) {
                for (int i = currIntX + 1; i < desIntX; i++) {
                    if (gameField1[i][currIntY] != null) {
                        return false;
                    }
                }
                return true;
            }
            else {
                return false;
            }
        }
        if (Math.abs(desIntY - currIntY) == Math.abs(desIntX - currIntX)) {
            if (Math.abs(desIntY - currIntY) == 1)
                return true;
            int valY;
            if (desIntY > currIntY) {
                valY = currIntY;
            }
            else {
                valY = desIntY;
            }
            if (desIntX > currIntX) {
                for (int i = currIntX + 1; i < desIntX; i++) {
                    valY++;
                    if (gameField1[i][valY] != null) {
                        return false;
                    }
                }
            }
            else {
                for (int i = desIntX + 1; i < currIntX; i++) {
                    valY++;
                    if (gameField1[i][valY] != null) {
                        return false;
                    }
                }
            }
            return true;
        }
        return true;
    }
    /**
     * @param locat Location where player desires to put piece into
     * @return boolean on whether locat is inside a typical chess field or not
     */
    public boolean checkIfValidBool(String locat) {
        if (locat.length() != 2)
            return false;
        String DesLet = locat.substring(0, 1);
        String desNum = locat.substring(1, 2);
        if (!(DesLet.matches("[a-zA-Z]")) || !(desNum.matches("[1-8]"))) {
            return false;
        }
        return true;
    }
    /**
     * creates chess gamefield
     */
    public void createGamefield() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                gameField1[i][j] = null;
            }
        }
        King initialBKing = new King("e8", "black");
        King initialWKing = new King("e1", "white");
        Queen initialBQueen = new Queen("d8", "black");
        Queen initialWQueen = new Queen("d1", "white");
        Bishop initialBBishop = new Bishop("c8", "black");
        Bishop initialBBishop2 = new Bishop("f8", "black");
        Bishop initialWBishop = new Bishop("c1", "white");
        Bishop initialWBishop2 = new Bishop("f1", "white");
        Knight initialBKnight = new Knight("b8", "black");
        Knight initialBKnight2 = new Knight("g8", "black");
        Knight initialWKnight = new Knight("b1", "white");
        Knight initialWKnight2 = new Knight("g1", "white");
        Rook initialBRook = new Rook("a8", "black");
        Rook initialBRook2 = new Rook("h8", "black");
        Rook initialWRook = new Rook("a1", "white");
        Rook initialWRook2 = new Rook("h1", "white");
        gameField1[0][0] = initialBRook;
        gameField1[0][7] = initialBRook2;
        gameField1[7][0] = initialWRook;
        gameField1[7][7] = initialWRook2;
        gameField1[0][1] = initialBKnight;
        gameField1[0][6] = initialBKnight2;
        gameField1[7][1] = initialWKnight;
        gameField1[7][6] = initialWKnight2;
        gameField1[0][2] = initialBBishop;
        gameField1[0][5] = initialBBishop2;
        gameField1[7][2] = initialWBishop;
        gameField1[7][5] = initialWBishop2;
        gameField1[0][3] = initialBQueen;
        gameField1[7][3] = initialWQueen;
        gameField1[0][4] = initialBKing;
        gameField1[7][4] = initialWKing;
        for (int i = 0; i < 8; i++) {
            Pawn newBlackPawn = new Pawn(Chess.convIntIntoChar(i) + "7", "black");
            newBlackPawn.setFirstMove(true);
            gameField1[1][i] = newBlackPawn;
            Pawn newWhitePawn = new Pawn(Chess.convIntIntoChar(i) + "2", "white");
            newWhitePawn.setFirstMove(true);
            gameField1[6][i] = newWhitePawn;
        }
    }
    /**
     * @param source Location where player desires to move piece from
     * @param desiredLoc Location where player desires to move piece into
     * @param trash Location of pawn to be eliminated once Enpassant finishes
     * @return Pieces[][] returns original gameboard with finished Enpassant or returns null
     */
    public Pieces[][] pawnSpecialEatingCase(String source, String desiredLoc, String trash) {
        if (source.length() != 2 || desiredLoc.length() != 2) {
            return null;
        }
        String desLet = trash.substring(0, 1);
        String desNum = trash.substring(1, 2);

        int desRow = 8 - Integer.parseInt(desNum);
        int desCol = desLet.charAt(0) - 'a';
        if (desRow > 8 || desRow < 0 || desCol > 8 || desCol < 0)
            return null;
        gameField1[desRow][desCol] = null;
        executePieceMove(source, desiredLoc, 'x');
        return gameField1;
    }
    /**
     * prepares both White and Black King for future methods
     */
    public void prepBothK() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (gameField1[i][j] != null) {
                    if (gameField1[i][j].toString().equals("bK")) {
                        blackKingLocat[0] = i;
                        blackKingLocat[1] = j;
                    }

                    else if (gameField1[i][j].toString().equals("wK")) {
                        whiteKingLocat[0] = i;
                        whiteKingLocat[1] = j;
                    }
                }
            }
        }
    }
    /**
     * prints out the Gamefield
     */
    public void generateGamefield() {
        String[][] visibleField = new String[9][9];
        for (int i = 0; i < 8; i++)
            visibleField[i][8] = Integer.toString(8 - i);
        for (int i = 0; i < 8; i++)
            visibleField[8][i] = " ".concat(Character.toString(Chess.convIntIntoChar(i)));
        visibleField[8][8] = "  ";
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (gameField1[i][j] != null) {
                    visibleField[i][j] = gameField1[i][j].toString();
                } else {
                    if ((i % 2 == 1 && j % 2 == 0) || (i % 2 == 0 && j % 2 == 1)) {
                        visibleField[i][j] = "##";
                    } else {
                        visibleField[i][j] = "  ";
                    }
                }
            }
        }
        for (int i = 0; i < 9; i++) {
            String lineATM = "";
            for (int j = 0; j < 9; j++) {
                if (j != 9 || j != 0)
                    lineATM = lineATM.concat(" ");
                lineATM = lineATM.concat(visibleField[i][j]);
            }
            System.out.println(lineATM);
        }

    }
    /**
     * @param givenSide The given side of chess game in Black or White
     * @return boolean on whether given side is in check
     */
    public boolean determineIfCheck(String givenSide) {
        prepBothK();
        int valueOfA =(int)'a';

        int obj1 = blackKingLocat[1] + valueOfA;
        int obj2 = whiteKingLocat[1] + valueOfA;
        char charObj1 =(char)obj1;
        char charObj2 = (char)obj2;
        int intObj2 = 8 - whiteKingLocat[0];
        int intObj1 = 8 - blackKingLocat[0];
        String colNum2 = Integer.toString(intObj1);
        String colNum3 = Integer.toString(intObj2);
        String thisBKing = charObj1 + colNum2;
        String thisWKing = charObj2 + colNum3;

        if (givenSide.equals("white")) {
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if (gameField1[i][j] != null) {
                        if ((gameField1[i][j].side.equals("white")) && (gameField1[i][j].isPathClear(thisBKing))
                                && (determineIfPathClear(gameField1[i][j], thisBKing))) {
                            return true;
                        }
                    }

                }
            }
        }
        else if (givenSide.equals("black")) {
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if (gameField1[i][j] != null) {
                        if ((gameField1[i][j].side.equals("black")) && (gameField1[i][j].isPathClear(thisWKing))
                                && (determineIfPathClear(gameField1[i][j], thisWKing))) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    /**
     * determines whether the current situation is in checkmate
     */
    public boolean determineIfCheckmate() {
        prepBothK();
        boolean whiteChecked = determineIfCheck("black");
        boolean blackChecked = determineIfCheck("white");
        int valueOfA = (int) 'a';

        if (whiteChecked) {

            int val4 = whiteKingLocat[1] + valueOfA;
            char charVal4 = (char) val4;
            int val5 = 8 - whiteKingLocat[0];
            String strVal5 = Integer.toString(val5);
            String thisWKing = charVal4 + strVal5;
            int x;
            int y;
            int[][] checkerField = new int[16][2];
            int checkerCount = 0;
            for (x = 0; x < 8; x++) {
                for (y = 0; y < 8; y++) {
                    if (gameField1[x][y] != null) {
                        if ((gameField1[x][y].side.equals("black")) && (gameField1[x][y].isPathClear(thisWKing))
                                && (determineIfPathClear(gameField1[x][y], thisWKing))) {
                            checkerField[checkerCount][0] = x;
                            checkerField[checkerCount][1] = y;
                            checkerCount++;
                        }
                    }
                }
            }
            if (checkerCount == 1) {
                y = checkerField[0][1];
                x = checkerField[0][0];
                int checkerInt1 = y + valueOfA;
                char checkerChar1 = (char) checkerInt1;
                int checkerChar2 = 8 - x;
                String checkerStr1 = Integer.toString(checkerChar2);
                String checker5 = checkerChar1 + checkerStr1;
                for (x = 0; x < 8; x++) {
                    for (y = 0; y < 8; y++) {
                        if (gameField1[x][y] != null) {
                            if ((gameField1[x][y].side.equals("white")) && (gameField1[x][y].isPathClear(checker5))
                                    && (determineIfPathClear(gameField1[x][y], checker5))) {
                                if((gameField1[x][y] instanceof Pawn) && checker5.charAt(0)==(char)(y + valueOfA)) {
                                    //System.out.println("Pawn cannot eat straight");
                                }
                                else{
                                Pieces[][] simCheckField = new Pieces[8][8];
                                Gamefield tempField = new Gamefield();
                                tempField.gameField1 = simCheckField;
                                for (int m = 0; m < 8; m++) {
                                    for (int n = 0; n < 8; n++) {
                                        simCheckField[m][n] = (Pieces) gameField1[m][n];
                                    }
                                }
                                int tankNum = y + valueOfA;
                                char charNum = (char) tankNum;
                                int tankRow = 8 - x;
                                String tankStr = Integer.toString(tankRow);
                                String tank5 = charNum + tankStr;
                                tempField.executePieceMove(tank5, checker5, 'x');
                                if (tempField.determineIfCheck("black")) {
                                    continue;
                                } else {
                                    return false;
                                }
                            }}
                        }
                    }
                }
            }
            return determineIfKingStuck("white");
        }
        else if (blackChecked) {
            int val7 = blackKingLocat[1] + valueOfA;
            char charVal7 = (char) val7;
            int val8 = 8 - blackKingLocat[0];
            String strVal8 = Integer.toString(val8);
            String thisBKing = charVal7 + strVal8;
            int x;
            int y;
            int[][] checkerField2 = new int[16][2];
            int checkerCount2 = 0;
            for (x = 0; x < 8; x++) {
                for (y = 0; y < 8; y++) {
                    if (gameField1[x][y] != null) {
                        if ((gameField1[x][y].side.equals("white")) && (gameField1[x][y].isPathClear(thisBKing))
                                && (determineIfPathClear(gameField1[x][y], thisBKing))) {
                            checkerField2[checkerCount2][0] = x;
                            checkerField2[checkerCount2][1] = y;
                            checkerCount2++;
                        }
                    }
                }
            }
            if (checkerCount2 == 1) {
                y = checkerField2[0][1];
                x = checkerField2[0][0];
                int checkerInt3 = y + valueOfA;
                char checkerChar3 = (char) checkerInt3;
                int checkerChar5 = 8 - x;
                String checkerStr5 = Integer.toString(checkerChar5);
                String checker9 = checkerChar3 + checkerStr5;
                for (x = 0; x < 8; x++) {
                    for (y = 0; y < 8; y++) {
                        if (gameField1[x][y] != null) {
                            if ((gameField1[x][y].side.equals("black")) && (gameField1[x][y].isPathClear(checker9))
                                    && (determineIfPathClear(gameField1[x][y], checker9))) {
                                if((gameField1[x][y] instanceof Pawn) && checker9.charAt(0)==(char)(y + valueOfA)) {
                                    //System.out.println("Pawn cannot eat straight");
                                }
                                else{
                                Pieces[][] simCheckField2 = new Pieces[8][8];
                                Gamefield tempField2 = new Gamefield();
                                tempField2.gameField1 = simCheckField2;
                                for (int m = 0; m < 8; m++) {
                                    for (int n = 0; n < 8; n++) {
                                        simCheckField2[m][n] = (Pieces) gameField1[m][n];
                                    }
                                }
                                int tankNum3 = y + valueOfA;
                                char tankchar3 = (char) tankNum3;
                                int tankRow3 = 8 - x;
                                String tankStr3 = Integer.toString(tankRow3);
                                String tank7 = tankchar3 + tankStr3;
                                tempField2.executePieceMove(tank7, checker9, 'x');
                                if (tempField2.determineIfCheck("white")) {
                                    continue;
                                } else {
                                    return false;
                                }
                            }}
                            else if((gameField1[x][y].side.equals("black")) && (gameField1[x][y] instanceof Pawn)){
                                if((checkerInt3 - x ==1 && checkerChar5 - y ==-1)|| (checkerInt3 - x ==-1 && checkerChar5 - y ==-1)){

                                }
                            }
                        }
                    }
                }
            }
            return determineIfKingStuck("black");
        } else {
            return false;
        }
    }
    /**
     * @param locat Location of piece to be chosen
     * @return Pieces The chosen piece in location locat is returned
     */
    public Pieces choosePiece(String locat) {
        if (locat.length() != 2)
            return null;
        String desLet = locat.substring(0, 1);
        String desNum = locat.substring(1, 2);

        int desRow = 8 - Integer.parseInt(desNum);
        int desCol = desLet.charAt(0) - 'a';
        if (desRow > 8 || desRow < 0 || desCol > 8 || desCol < 0)
            return null;
        return gameField1[desRow][desCol];

    }
    /**
     * @param desiredLoc The desired location by Player to move piece into
     * @param optionalProm Extra argument for promotion
     * @param source The desired location by Player to move piece from
     * @return returns whether move execution was successful or not
     */
    public boolean executePieceMove(String source, String desiredLoc, char optionalProm) {
        Pieces[][] simCheckfield = new Pieces[8][8];
        Gamefield tempField = new Gamefield();
        tempField.gameField1 = simCheckfield;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                simCheckfield[i][j] = (Pieces) gameField1[i][j];
            }
        }
        if (source.length() != 2 || desiredLoc.length() != 2) {
            return false;
        }
        String desLet5 = source.substring(0, 1);
        String desNum5 = source.substring(1, 2);
        int desRow5 = 8 - Integer.parseInt(desNum5);
        int desCol5 = desLet5.charAt(0) - 'a';
        if (desRow5 > 8 || desRow5 < 0 || desCol5 > 8 || desCol5 < 0)
            return false;
        String desLet9 = desiredLoc.substring(0, 1);
        String desNum9 = desiredLoc.substring(1, 2);
        int desRow9 = 8 - Integer.parseInt(desNum9);
        int desCol9 = desLet9.charAt(0) - 'a';
        if (desRow9 > 8 || desRow9 < 0 || desCol9 > 8 || desCol9 < 0){
            return false;
        }
        switch(optionalProm) {
            case 'Q':
                Queen ProToQ = new Queen(desiredLoc, gameField1[desRow5][desCol5].side);
                simCheckfield[desRow9][desCol9] = ProToQ;
                simCheckfield[desRow5][desCol5] = null;
                if (gameField1[desRow5][desCol5].side.equals("white")) {
                    if (tempField.determineIfCheck("black"))
                        return false;
                    this.gameField1 = simCheckfield;
                    return true;
                } else {
                    if (tempField.determineIfCheck("white"))
                        return false;
                    this.gameField1 = simCheckfield;
                    return true;
                }
            case 'N':
                Knight ProToN = new Knight(desiredLoc, gameField1[desRow5][desCol5].side);
                simCheckfield[desRow9][desCol9] = ProToN;
                simCheckfield[desRow5][desCol5] = null;
                if (gameField1[desRow5][desCol5].side.equals("white")) {
                    if (tempField.determineIfCheck("black"))
                        return false;
                    this.gameField1 = simCheckfield;
                    return true;
                } else {
                    if (tempField.determineIfCheck("white"))
                        return false;
                    this.gameField1 = simCheckfield;
                    return true;
                }
            case 'B':
                Bishop ProToB = new Bishop(desiredLoc, gameField1[desRow5][desCol5].side);
                simCheckfield[desRow9][desCol9] = ProToB;
                simCheckfield[desRow5][desCol5] = null;
                if (gameField1[desRow5][desCol5].side.equals("white")) {
                    if (tempField.determineIfCheck("black"))
                        return false;
                    this.gameField1 = simCheckfield;
                    return true;
                } else {
                    if (tempField.determineIfCheck("white"))
                        return false;
                    this.gameField1 = simCheckfield;
                    return true;
                }
            case 'R':
                Rook ProToR = new Rook(desiredLoc, gameField1[desRow5][desCol5].side);
                simCheckfield[desRow9][desCol9] = ProToR;
                simCheckfield[desRow5][desCol5] = null;
                if (gameField1[desRow5][desCol5].side.equals("white")) {
                    if (tempField.determineIfCheck("black"))
                        return false;
                    this.gameField1 = simCheckfield;
                    return true;
                } else {
                    if (tempField.determineIfCheck("white"))
                        return false;
                    this.gameField1 = simCheckfield;
                    return true;
                }
            default:
                if (simCheckfield[desRow5][desCol5] instanceof Pawn) {
                    Pawn tPawn = (Pawn) simCheckfield[desRow5][desCol5];
                    tPawn.firstMove = false;
                    if (Math.abs(desRow5 - desRow9) == 2) {
                        tPawn.advancedTwo = true;
                    } else {
                        tPawn.advancedTwo = false;
                    }
                    simCheckfield[desRow5][desCol5] = tPawn;
                } else if (simCheckfield[desRow5][desCol5] instanceof King) {
                    King tKing = (King) simCheckfield[desRow5][desCol5];
                    tKing.firstMove = false;
                    simCheckfield[desRow5][desCol5] = tKing;
                } else if (simCheckfield[desRow5][desCol5] instanceof Rook) {
                    Rook tRook = (Rook) simCheckfield[desRow5][desCol5];
                    tRook.firstMove = false;
                    simCheckfield[desRow5][desCol5] = tRook;
                }
                simCheckfield[desRow9][desCol9] = simCheckfield[desRow5][desCol5];
                simCheckfield[desRow9][desCol9].locNow = desiredLoc;
                simCheckfield[desRow5][desCol5] = null;
                if (this.gameField1[desRow5][desCol5].side.equals("white")) {
                    if (tempField.determineIfCheck("black")) {
                        return false;
                    }
                    this.gameField1 = simCheckfield;
                    return true;
                } else {
                    if (tempField.determineIfCheck("white")) {
                        return false;
                    }
                    this.gameField1 = simCheckfield;
                    return true;
                }
        }
    }
    /**
     * @param currLocOnField The desired location by Player to move piece from in int array
     * @param desiredLocOnField The desired location by Player to move piece into in int array
     */
    public void executePieceMove(int[] currLocOnField, int[] desiredLocOnField) {
        gameField1[currLocOnField[0]][currLocOnField[1]]=gameField1[desiredLocOnField[0]][desiredLocOnField[1]];
    }
}
