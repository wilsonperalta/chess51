package src.chess;

/**
 * @author Beomsik Kim
 * @author Wilson Peralta
 */

/**
 * Class knight extends the Pieces class with specific functions for functionality
 * to the knight piece on the board
 * @param locat string with desired location
 * @param givenSide what color of the piece
 */

public class Knight extends Pieces {
	public Knight(String locat, String givenSide){
		this.side=givenSide;
		locNow=locat;
	}
/**
 * 
 * method isPathClear checks whether desired path between src and dest is clear
 * @param string f_pos
 * @returns a boolean true or false accordingly
 */

	public boolean isPathClear(String f_pos){
		int[] givenArr=convStrIntoIntArr(f_pos);
		int[] currArr=convStrIntoIntArr(this.locNow);
		if(!validMove(givenArr)){
			return false;
		}
		else if(givenArr[0]==currArr[0]-2){
			if((givenArr[1]==currArr[1]+1)||(givenArr[1]==currArr[1]-1)){
				return true;
			}
		}
		else if(givenArr[0]==currArr[0]+1){
			if((givenArr[1]==currArr[1]+2)||(givenArr[1]==currArr[1]-2)){
				return true;
			}
		}
		else if(givenArr[0]==currArr[0]+2){
			if((givenArr[1]==currArr[1]+1)||(givenArr[1]==currArr[1]-1)){
				return true;
			}
		}
		else if(givenArr[0]==currArr[0]-1){
			if((givenArr[1]==currArr[1]+2)||(givenArr[1]==currArr[1]-2)){
				return true;
			}
		}
		else if(currArr[0]==givenArr[0]){
			if(currArr[1]==givenArr[1]){
				return false;
			}
		}
		else{
			return false;
		}
		return false;
	}

	/**
	 * toString method 
	 * @return proper color of the piece
	 */
	public String toString(){
		if (side.equals("black")){
			return "bN";
		}
		else{
			return "wN";
		}
	}

}
